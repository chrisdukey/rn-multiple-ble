package com.activimeter.ble;

import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This is a simple blocking queue of bytes implemented as a blocking queue of byte buffers using standard Java Concurrency classes
 */
public class ByteQueue {
    private BlockingQueue<byte[]> pendingData = new LinkedBlockingQueue<byte[]>();
    private byte[] currentData = null;
    private int currentIndex = 0;
    private static final String TAG = "BLEPlugin";

    public ByteQueue() {}

    public byte next() throws InterruptedException {
        fetchCurrentData();

        return currentData[currentIndex++];
    }

    private void fetchCurrentData() throws InterruptedException {
        if (null == currentData || currentIndex >= currentData.length) {
            currentIndex = 0;
            currentData = pendingData.take();
            Log.d(TAG, "currentData is " + bytesToHex(currentData));
            fetchCurrentData(); // recursive call to make sure we got a non-emply buffer or something bad did not happen
        }
    }

    public void put(byte[] buffer) throws InterruptedException {
        Log.d(TAG, "In put of ByteQueue.");
        pendingData.put(buffer);
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
