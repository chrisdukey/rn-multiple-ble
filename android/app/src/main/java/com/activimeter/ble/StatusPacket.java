package com.activimeter.ble;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class StatusPacket extends PdiPacket {
    public enum ChargingState {
        Discharged,
        Charging,
        Charged
    }

    public StatusPacket(byte[] _bytes) {
        super(PDI_CMD_STATUS, _bytes);

        model = data.get();
        chargerState = data.get();
        int rawBatt = data.getShort() & 0xFFFF;
        vBatt = 1.0f * rawBatt / 100;
    }
    private static final String DEBUGTAG = "SP10C";

    byte model;
    byte chargerState;
    float vBatt;

    public byte getModel() {
        return model;
    }

    public ChargingState getChargerState() {
        switch (chargerState) {
            case 0:
                return ChargingState.Discharged;
            case 1:
                return ChargingState.Charging;
            default:
                return ChargingState.Charged;
        }
    }

    public float getVBatt() {
        return vBatt;
    }

    // Convert packet to JSON for return to Cordova
    public JSONObject toJSON()
    {
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        JSONObject json;
        try{
            json = new JSONObject(jsonString);
        }
        catch (JSONException ex){
            Log.e(DEBUGTAG, "Failed to convert StatusPacket to JSONObject");
            json = null;
        }

        return json;
    }
}
