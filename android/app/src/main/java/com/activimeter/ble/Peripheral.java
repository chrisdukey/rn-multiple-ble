package com.activimeter.ble;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.os.Trace;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;


import com.facebook.binaryresource.ByteArrayBinaryResource;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;

import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;


import org.json.JSONArray;
import org.json.JSONObject;

import org.json.JSONException;

import java.util.ArrayList;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;

/**
 * Created by chris on 31/12/2016.
 */
public class Peripheral extends BluetoothGattCallback {
    private static final String TAG = "BLEPlugin";
    private ReactContext _reactContext;
    private BluetoothDevice device;

    private DeviceMonitor deviceMonitor;

    private Promise connectPromise;
    private Promise readPromise;
    private Promise readRSSIPromise;
    private Promise writePromise;

    private boolean connected = false;


    private BluetoothGatt gatt;

    private final boolean start_streaming = true;
    private final boolean stop_streaming = false;
    private final boolean on = true;
    private final boolean off = false;

    //Client Configuration UUID for notifying/indicating
    private final UUID clientConfigurationDescriptorUuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    public final static UUID UUID_SP_SERVICE = UUID.fromString("01000000-0000-0000-0000-000000000080");
    public final static UUID UUID_SP_WRITE_CHARACTERISTIC = UUID.fromString("04000000-0000-0000-0000-000000000080");

    private Activity currentActivity;
    protected ExecutorService threadPool;
    private byte[] advertisingData;
    private int advertisingRSSI;
    private int numRecordsInLog;

    private DeviceMonitor.JSONArrayQueue streamPacketQueue;
    private final static int PACKETS_IN_QUEUE = 100;


    public Peripheral(BluetoothDevice device, ReactContext reactContext) {
        this.device = device;
        this._reactContext = reactContext;
        threadPool = Executors.newCachedThreadPool();

    }

    public Peripheral(BluetoothDevice device, int advertisingRSSI, byte[] scanRecord, ReactContext reactContext) {

        this(device, reactContext);
        this.advertisingRSSI = advertisingRSSI;
        this.advertisingData = scanRecord;


    }

    public BluetoothGatt getGatt() {
        return gatt;
    }

    public void connect(Promise promise, Activity activity) throws JSONException {
        if (!connected) {
            this.currentActivity = activity;
            BluetoothDevice device = getDevice();
            this.connectPromise = promise;
            gatt = device.connectGatt(activity, false, this);
        } else {
            if (gatt != null) {
                promise.resolve(ReactNativeJson.convertJsonToMap(JSONObjects.asDevice(gatt, getBluetoothManager())));

            } else
                promise.reject("PERIPHERAL_CONNECT", "BluetoothGatt is null");
        }

    }

    public void disconnect() {
        connectPromise = null;
        connected = false;
        if (gatt != null) {
            try {

                if (deviceMonitor != null) {
                    deviceMonitor.togglePacketProcessor(off);
                    deviceMonitor = null;
                }

                gatt.disconnect();
                gatt.close();
                Log.d(TAG, "Disconnect");
                JSONObject obj = JSONObjects.asDevice(gatt, getBluetoothManager());
                gatt = null;
                sendEvent(_reactContext, "deviceDisconnect", obj);

            } catch (Exception e) {
                JSONObject obj = JSONObjects.asDevice(gatt, getBluetoothManager());
                sendEvent(_reactContext, "deviceDisconnect", obj);
                Log.d(TAG, "Error on disconnect", e);
            }
        } else
            Log.d(TAG, "GATT is null");
    }

    public void controlStreaming(boolean enable) {
        deviceMonitor.controlStreaming(enable);
    }

    public void controlStreaming(int streamingPeriodMs) {
        deviceMonitor.controlStreaming(streamingPeriodMs);
    }

    public void deviceGetStatus() {
        deviceMonitor.deviceGetStatus();
    }

    public void loggerGetStatus() {
        deviceMonitor.loggerGetStatus();
    }

    public void loggerGetRecords() {
        deviceMonitor.loggerGetRecords();
    }

    public void clearLog() {
        deviceMonitor.clearLog();
    }

    public void controlLogging(boolean enable) {
        deviceMonitor.controlLogging(enable);
    }

    public void setRTC() {
        deviceMonitor.setRTC();
    }

    public void getRTC() {
        deviceMonitor.getRTC();
    }

    public boolean isConnected() {
        return connected;
    }

    private BluetoothManager getBluetoothManager() {
        return (BluetoothManager) currentActivity.getSystemService(Context.BLUETOOTH_SERVICE);
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable JSONObject obj) {

        if (obj != null) {
            //BundleJSONConverter bjc = new BundleJSONConverter();
            try {
                //Bundle bundle = bjc.convertToBundle(obj);
                //WritableMap map = Arguments.fromBundle(bundle);
                //sendEvent(_reactContext, eventName, map);

                sendEvent(_reactContext, eventName, ReactNativeJson.convertJsonToMap(obj));

            } catch (JSONException ex) {
                Log.e(TAG, "JSONException converting JSON to Map in sendEvent");
            }
        }
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable String params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }


    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableArray params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    @Override
    public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
        final String address = gatt.getDevice().getAddress();


        Log.d(TAG, "onConnectionStateChange from " + status + " to " + newState + " on peripheral:" + device.getAddress());

        this.gatt = gatt;

        if (newState == BluetoothProfile.STATE_CONNECTED) {
            Log.d(TAG, "Sensor " + address + " connected");
            connected = true;
            gatt.discoverServices();

            deviceMonitor = new DeviceMonitor(gatt, _reactContext);


            sendEvent(_reactContext, "deviceConnect", JSONObjects.asDevice(gatt, getBluetoothManager()));

        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            //TODO Need to ensure streaming has stopped or disconnect causes crash
            // Stop threads listening for incoming packets from SP10-C
            //connectedDeviceMonitors.get(address).togglePacketProcessor(off);

            connected = false;

            Log.d(TAG, "Sensor " + address + " disconnected");

            if (deviceMonitor != null) {
                deviceMonitor.togglePacketProcessor(off);
                deviceMonitor = null;
            }

            JSONObject obj = JSONObjects.asDevice(gatt, getBluetoothManager());
            sendEvent(_reactContext, "deviceDisconnect", obj);

            gatt.close();


        }

    }


    @Override
    public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
        final String address = gatt.getDevice().getAddress();
        super.onServicesDiscovered(gatt, status);

        Trace.beginSection("Discovering services for device " + gatt.getDevice().getAddress());
        try {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                for (BluetoothGattService service : gatt.getServices()) {
                    for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                        characteristic.getDescriptors();
                    }
                }

                Trace.beginSection("Success callback for device " + address);
                try {
                    sendEvent(_reactContext, "deviceConnect", JSONObjects.asDevice(gatt, getBluetoothManager()));

                    this.connectPromise.resolve(ReactNativeJson.convertJsonToMap(JSONObjects.asDevice(gatt, getBluetoothManager())));
                    this.connectPromise = null;

                    // Turn on the packet processor to listen for packets from the device
                    deviceMonitor.togglePacketProcessor(on);

                } catch (JSONException ex) {
                    Log.e(TAG, "JSON exception in onServicesDiscovered");
                    this.connectPromise.reject(ex);
                    this.connectPromise = null;
                } finally {
                    Trace.endSection();
                }
                Log.d(TAG, "Services discovered for device " + gatt.getDevice().getAddress());
            } else {
                sendEvent(_reactContext, "deviceConnect", JSONObjects.asError(new Exception("Device discovery failed")));
            }

        } finally {
            Trace.endSection();
        }

    }


    @Override
    public void onCharacteristicRead(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {

        if (status == BluetoothGatt.GATT_SUCCESS) {

            WritableMap map = Arguments.createMap();
            map.putString("characteristic", Base64.encodeToString(characteristic.getValue(), Base64.NO_WRAP));
            sendEvent(_reactContext, "onCharacteristicRead", map);
        } else {
            //TODO Do we want to return an error if the read failed?
            //callback.error(JSONObjects.asError(new Exception("Failed to read characteristic")));
        }

    }


    @Override
    public void onCharacteristicChanged(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {

        if (characteristic.getService().getUuid().equals(UUID_SP_SERVICE) || characteristic.getUuid().equals(UUID_SP_WRITE_CHARACTERISTIC)) {
            final byte[] data = characteristic.getValue();

            try {
                deviceMonitor.pushData(data);
            } catch (Exception ex) {
                Log.e(TAG, "Exception pushing data to queue for device " +
                        gatt.getDevice().getAddress() + ". Exception was " + ex.toString());
            }


        } else {
            Log.w(TAG, "Data from unknown characteristic received");
        }

        //sendEvent(_reactContext, "onCharacteristicChanged", JSONObjects.asCharacteristic(characteristic));


    }


    @Override
    public void onCharacteristicWrite(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {

        deviceMonitor.dequeueCommand();

        if (status == BluetoothGatt.GATT_SUCCESS) {
            sendEvent(_reactContext, "onCharacteristicWrite", JSONObjects.asCharacteristic(characteristic));
        } else {
            sendEvent(_reactContext, "onCharacteristicWrite", JSONObjects.asError(new Exception("Failed to write characteristic")));
        }

    }


    @Override
    public void onDescriptorRead(final BluetoothGatt gatt, final BluetoothGattDescriptor descriptor, final int status) {

        if (status == BluetoothGatt.GATT_SUCCESS) {
            sendEvent(_reactContext, "onDescriptorRead", JSONObjects.asDescriptor(descriptor));
        } else {
            sendEvent(_reactContext, "onDescriptorRead", JSONObjects.asError(new Exception("Failed to read descriptor " + descriptor.getUuid().toString())));
        }

    }


    @Override
    public void onDescriptorWrite(final BluetoothGatt gatt, final BluetoothGattDescriptor descriptor, final int status) {
        if (status == BluetoothGatt.GATT_SUCCESS) {
            sendEvent(_reactContext, "onDescriptorWrite", JSONObjects.asDescriptor(descriptor));
        } else {
            sendEvent(_reactContext, "onDescriptorWrite", JSONObjects.asError(new Exception("Failed to write descriptor " + descriptor.getUuid().toString())));
        }
    }


    @Override
    public void onReadRemoteRssi(final BluetoothGatt gatt, final int rssi, final int status) {

        BluetoothManager bluetoothManager = (BluetoothManager) currentActivity.getSystemService(Context.BLUETOOTH_SERVICE);
        if (status == BluetoothGatt.GATT_SUCCESS) {

            sendEvent(_reactContext, "onReadRemoteRssi", JSONObjects.asDevice(gatt, bluetoothManager, rssi));
        } else {
            sendEvent(_reactContext, "onReadRemoteRssi", JSONObjects.asError(new Exception("Received an error after attempting to read RSSI for device " + gatt.getDevice().getAddress())));
        }
    }

    protected ExecutorService getThreadPool() {
        return this.threadPool;
    }


    private class DeviceMonitor {
        ByteQueue queue;
        PdiParser pdiParser;

        private Thread pdiPacketProcessor = new PdiPacketProcessor();
        private Thread pdiErrorMonitor = new PdiErrorMonitor();
        private static final String BLETAG = "BLEPlugin";
        private static final boolean on = true;
        private static final boolean off = false;
        private String deviceAddress;
        private boolean isStreaming;
        private boolean isLogging;
        private LogStatusPacket currentLogStatus = null;
        private boolean loggerGettingRecords = false;

        LinkedList<BluetoothCommand> mCommandQueue = new LinkedList<BluetoothCommand>();
        //Command Operation executor - will only run one at a time
        Executor mCommandExecutor = Executors.newSingleThreadExecutor();
        //Semaphore lock to coordinate command executions, to ensure only one is
        //currently started and waiting on a response.
        Semaphore mCommandLock = new Semaphore(1, true);

        private BluetoothGatt mBluetoothGatt;

        private ReactContext _reactContext;


        public DeviceMonitor(BluetoothGatt mBluetoothGatt, ReactContext reactContext) {
            Log.d(TAG, "SP10C: DeviceMon created");
            this._reactContext = reactContext;
            this.deviceAddress = mBluetoothGatt.getDevice().getAddress();
            this.mBluetoothGatt = mBluetoothGatt;

            if (queue == null) {
                queue = new ByteQueue();
            }

            if (pdiParser == null) {
                pdiParser = new PdiParser(queue);
            }

        }


        public void loggerGetStatus() {
            toggleWriteCharNotifications(on);

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    Log.d(TAG, "Getting logger status");
                    sendCommand(PdiPacket.PDI_CMD_LOGGETSTATUS, (byte) 0x0);
                }
            });

        }

        public void deviceGetStatus() {
            toggleWriteCharNotifications(on);

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    Log.d(TAG, "Getting device status");
                    sendCommand(PdiPacket.PDI_CMD_STATUS, (byte) 0x0);
                }
            });

        }

        public void loggerGetRecords() {


            // BLE Commands are synchronous so we have to be sure that the previous command has
            // completed before calling the next one otherwise subsequent ones are ignored.
            // Queuing here is based on https://github.com/freewheeling/BluetoothQueue

            loggerGettingRecords = true;

            toggleWriteCharNotifications(on);

            // Once the LogStatusPacket arrives the loggerGetRecordPackets method is called
            // and loops through all records in the log
            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    Log.d(TAG, "Getting number of records from log status");
                    sendCommand(PdiPacket.PDI_CMD_LOGGETSTATUS, (byte) 0x00);
                }
            });

        }

        public void loggerGetRecordPackets(int noOfRecords) {

            loggerGettingRecords = false;

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    Log.d(TAG, "Command for first record");
                    sendCommand(PdiPacket.PDI_CMD_LOGGETFIRSTRECORD, (byte) 0x00);
                }
            });


            for (int i = 1; i <= noOfRecords; i++) {
                Log.d(TAG, "Val of i is " + i);
                final byte byteVal = new Integer(i).byteValue();
                final int j = i;

                queueCommand(new BluetoothCommand() {
                    @Override
                    public void execute() {
                        super.execute();
                        Log.d(TAG, "Command for record " + j);
                        sendCommand(PdiPacket.PDI_CMD_LOGGETRECORD, (byte) 0x00);
                    }
                });
            }

        }

        public void queueCommand(BluetoothCommand command) {
            synchronized (mCommandQueue) {
                mCommandQueue.add(command); //Add to end of stack
                //Schedule a new runnable to process that command (one command at a time executed only)
                ExecuteCommandRunnable runnable = new ExecuteCommandRunnable(command);
                mCommandExecutor.execute(runnable);
            }
        }

        //Remove the current command from the queue, and release the lock
        //signalling the next queued command (if any) that it can start
        protected void dequeueCommand() {
            mCommandQueue.pop();
            mCommandLock.release();
        }

        public void controlStreaming(int streamingPeriodMs) {

            controlStreaming(true);

            if (streamingPeriodMs >= 0) {

                Handler mHandler = new Handler();

                // Stop streaming after the period defined by the user
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                // Stop the stream
                                controlStreaming(false);
                            }
                        });

                    }
                }, streamingPeriodMs);

            }
        }

        public void controlStreaming(final boolean on) {

            toggleWriteCharNotifications(on);

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    Log.d(TAG, "Device streaming: " + on);
                    sendCommand(PdiPacket.PDI_CMD_STREAMENABLE, on ? (byte) 0x01 : (byte) 0x00);
                }
            });


            isStreaming = on;

            streamPacketQueue = new JSONArrayQueue(PACKETS_IN_QUEUE);

            JSONObject streamingToggleMessage = new JSONObject();
            try {
                streamingToggleMessage.put("device", deviceAddress);
                streamingToggleMessage.put("isStreaming", isStreaming);

                sendEvent(_reactContext, "deviceIsStreaming", streamingToggleMessage);
            } catch (JSONException ex) {
                Log.e(TAG, "Couldn't put message in JSONObect. WTF?!");
            }


            Log.d(TAG, "SP10C: DeviceMon.ctlStrm called");
        }

        public void clearLog() {
            toggleWriteCharNotifications(on);

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    Log.d(TAG, "Clearing log");
                    sendCommand(PdiPacket.PDI_CMD_LOGCLEAR, (byte) 0x00);
                }
            });
        }

        public void controlLogging(final boolean on) {

            toggleWriteCharNotifications(on);

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    sendCommand(PdiPacket.PDI_CMD_LOGENABLE, on ? (byte) 0x01 : (byte) 0x00);
                }
            });


            isLogging = on;


            JSONObject loggingToggleMessage = new JSONObject();
            try {
                loggingToggleMessage.put("device", deviceAddress);
                loggingToggleMessage.put("isLogging", isLogging);

                sendEvent(_reactContext, "deviceIsLogging", loggingToggleMessage);
            } catch (JSONException ex) {
                Log.e(TAG, "Couldn't put message in JSONObect. WTF?!");
            }


            Log.d(TAG, "SP10C: DeviceMon.ctlLogging called");
        }


        // Set the Real Time Clock on the sensor to the current time on the phone
        public void setRTC() {
            Calendar calendar = Calendar.getInstance();
            //Time (SS)
            byte seconds = ((Integer) calendar.get(Calendar.SECOND)).byteValue();
            //Time (MM)
            byte minutes = ((Integer) calendar.get(Calendar.MINUTE)).byteValue();
            //Time (HH)
            byte hours = ((Integer) calendar.get(Calendar.HOUR_OF_DAY)).byteValue();
            //Date (Date)
            byte day = ((Integer) calendar.get(Calendar.DAY_OF_MONTH)).byteValue();
            //Date & Time is set. Not sure what this is for but always seems to be 1
            byte dateTimeIsSet = 1;
            //Date (Month)
            byte month = ((Integer) (calendar.get(Calendar.MONTH) + 1)).byteValue(); // Months start at zero
            // Date (Year)
            byte year = ((Integer) (calendar.get(Calendar.YEAR) - 2000)).byteValue();

            byte[] pData = {seconds, minutes, hours, day, dateTimeIsSet, month, year};

            //byte[] pData = {(byte)20, (byte)2, (byte)1, (byte)19, (byte)1, (byte)4 , (byte)17};

            sendPacket(PdiPacket.BLE_CMD_SETRTC, pData);

        }

        public void getRTC() {
            toggleWriteCharNotifications(on);

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    Log.d(TAG, "Getting realtime clock value");
                    sendCommand(PdiPacket.BLE_CMD_GETRTC, (byte) 0x0);
                }
            });
        }


        private void toggleWriteCharNotifications(boolean on) {
            BluetoothGattService service = getService();
            if (null == service) {
                Log.e(TAG, "Problem toggling notications to " + on + ": no GATT service. Device " + deviceAddress);
                sendEvent(_reactContext, "bluetoothErrors", JSONObjects.asError(new Exception("Problem toggling notications to " + on + ": no GATT service. Device " + deviceAddress)));
                return;
            }

            for (BluetoothGattCharacteristic ch : getService().getCharacteristics()) {
                if (!ch.getUuid().equals(UUID_SP_WRITE_CHARACTERISTIC)) {
                    mBluetoothGatt.setCharacteristicNotification(ch, on);
                }
            }
        }

        public void togglePacketProcessor(boolean on) {
            if (on) {

                getThreadPool().execute(pdiPacketProcessor);
                getThreadPool().execute(pdiErrorMonitor);
            } else {
                pdiPacketProcessor.interrupt();
                pdiErrorMonitor.interrupt();
            }
        }

        // This is the code that is in the Byte class in Java 8. Copied here
        // as I appear to be using Java 7
        private int toUnsignedInt(byte x) {
            return ((int) x) & 0xff;
        }

        private void sendByte(final byte data) {

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    Log.d("BLE PACKET", "BLEPACKET BYTE " + data + " UNSIGNED " + toUnsignedInt(data) + " HEX " + Integer.toHexString(toUnsignedInt(data)));
                    sendCommand(data, (byte) 0x00);
                }
            });
        }

        private void sendPacket(final byte Cmd, byte[] pData) {

            List<Byte> currentPacket = new ArrayList<>();


            Log.d("BLE PACKET", "BLEPACKET " + pData.toString());
            byte c;
            byte chksum;
            int pDataIndex = 0;
            int Length = pData.length;

             /*---- always start with SOP ----*/
            //currentPacket.add(PdiPacket.PDI_SOP);

             /*---- send packet command ----*/
            chksum = Cmd;
            currentPacket.add(Cmd);


           /*---- send packet data (if any) ----*/

            while (Length-- != 0) {
                c = pData[pDataIndex++];

            /*---- check if we need to insert a stuff byte ----*/
                if (c == PdiPacket.PDI_SOP || c == PdiPacket.PDI_EOP || c == PdiPacket.PDI_STF) {
                    chksum = (byte) (toUnsignedInt(chksum) + PdiPacket.PDI_STF);
                    currentPacket.add(PdiPacket.PDI_STF);
                }
                chksum = (byte) (toUnsignedInt(chksum) + toUnsignedInt(c));
                currentPacket.add(c);
            }

            /*---- always end with checksum and EOP ----*/
            c = (byte) (0 - toUnsignedInt(chksum));
            if (c == PdiPacket.PDI_SOP || c == PdiPacket.PDI_EOP || c == PdiPacket.PDI_STF) {
                chksum = (byte) (toUnsignedInt(chksum) + toUnsignedInt(PdiPacket.PDI_STF));
                currentPacket.add(PdiPacket.PDI_STF);
                c = (byte) (0 - toUnsignedInt(chksum));
            }

            currentPacket.add(c);
            //currentPacket.add( PdiPacket.PDI_EOP );

            final byte[] finalPacket = new byte[currentPacket.size()];
            for (int i = 0; i < currentPacket.size(); i++) {
                finalPacket[i] = currentPacket.get(i).byteValue();
            }

            Log.e("BLE", "FINALPACKET " + Byte.toString(finalPacket[0]) + " " + Byte.toString(finalPacket[1]));

            queueCommand(new BluetoothCommand() {
                @Override
                public void execute() {
                    super.execute();
                    sendCommand(finalPacket);
                }
            });

        }

        private void sendCommand(byte[] command) {
            BluetoothGattCharacteristic writeCh = getCharacteristic(UUID_SP_WRITE_CHARACTERISTIC);

            if (null == writeCh) {
                Log.e(TAG, "Problem sending command: no characteristic");
                return;
            }

            writeCh.setValue(command);
            mBluetoothGatt.writeCharacteristic(writeCh);
        }

        private void sendCommand(byte command) {
            BluetoothGattCharacteristic writeCh = getCharacteristic(UUID_SP_WRITE_CHARACTERISTIC);

            if (null == writeCh) {
                Log.e(TAG, "Problem sending command: no characteristic");
                return;
            }

            writeCh.setValue(new byte[]{command});
            mBluetoothGatt.writeCharacteristic(writeCh);
        }

        private void sendCommand(byte command, byte color) {
            BluetoothGattCharacteristic writeCh = getCharacteristic(UUID_SP_WRITE_CHARACTERISTIC);

            if (null == writeCh) {
                Log.e(TAG, "Problem sending command: no characteristic");
                return;
            }

            writeCh.setValue(new byte[]{command, color});
            mBluetoothGatt.writeCharacteristic(writeCh);
        }

        private BluetoothGattCharacteristic getCharacteristic(UUID uuid) {
            BluetoothGattService spService = getService();
            if (spService == null) return null;

            return spService.getCharacteristic(uuid);
        }

        private BluetoothGattService getService() {
            if (null == mBluetoothGatt) {
                Log.e(TAG, "BluetoothGatt");
                return null;
            }

            BluetoothGattService spService = mBluetoothGatt.getService(UUID_SP_SERVICE);
            if (null == spService) {
                Log.e(TAG, "no service");
                return null;
            }
            return spService;
        }

        public void pushData(byte[] data) {
            try {
                Log.d(TAG, "In pushData for device " + deviceAddress);
                queue.put(data);
                pdiParser.setDeviceAddress(deviceAddress);

            } catch (InterruptedException e) {
                Log.e(TAG, "Failed to push data for processing");
            }
        }


        class PdiPacketProcessor extends Thread {
            /**
             * Main packet handling loop goes here. Separate thread waits for bytes to arrive, collects into packets and
             * pumps to Plugin.
             */
            @Override
            public void run() {
                Log.d(TAG, "PdiPacketProcessor started");
                try {
                    while (!Thread.interrupted()) {
                        final PdiPacket packet = pdiParser.next();
                        FillData(packet);
                        Log.i(TAG, "Data received");
                    }
                } catch (InterruptedException e) {
                    Log.d(TAG, "PdiPacketProcessor interrupted");
                    e.printStackTrace();
                }
            }
        }

        class PdiErrorMonitor extends Thread {
            @Override
            public void run() {
                Log.d(TAG, "PdiErrorMonitor started");
                try {
                    int lastErrorCount = 0;
                    while (!Thread.interrupted()) {
                        final int newErrorCount = pdiParser.getErrorCount();
                        if (newErrorCount > lastErrorCount)
                            Log.i(TAG, "Error count is " + newErrorCount);
                        lastErrorCount = newErrorCount;
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    Log.d(TAG, "PdiErrorMonitor interrupted");
                }
            }
        }

        //Runnable to execute a command from the queue
        class ExecuteCommandRunnable implements Runnable {
            BluetoothCommand mCommand;

            public ExecuteCommandRunnable(BluetoothCommand command) {
                mCommand = command;
            }

            @Override
            public void run() {
                //Acquire semaphore lock to ensure no other operations can run until this one completed
                mCommandLock.acquireUninterruptibly();
                //Tell the command to start itself.
                mCommand.execute();
            }
        }

        ;

        private void FillData(PdiPacket packet) {
            if (null == packet)
                return;
            if (packet instanceof StreamingPacket)
                FillStreamingData((StreamingPacket) packet);
            else if (packet instanceof VersionPacket)
                FillVersionData((VersionPacket) packet);
            else if (packet instanceof StatusPacket)
                FillStatusData((StatusPacket) packet);
            else if (packet instanceof LogStatusPacket)
                FillLogStatusData((LogStatusPacket) packet);
            else if (packet instanceof LogRecordPacket)
                FillLogRecordData((LogRecordPacket) packet);
            else if (packet instanceof ClockPacket)
                FillClockData((ClockPacket) packet);
        }

        private void FillClockData(ClockPacket packet) {
            Log.d(TAG, packet.toJSON().toString());
            sendEvent(_reactContext, "packetListener", packet.toJSON());
        }


        private void FillLogRecordData(final LogRecordPacket packet) {
            Log.d(TAG, packet.toJSON().toString());

            if (packet.hasTimeDate() && packet.hasTimestamp() &&
                    packet.hasGyros() && packet.hasAccels()) {

                // TODO Implement this
            }
        }

        private void FillLogStatusData(LogStatusPacket packet) {
            Log.d(TAG, packet.toJSON().toString());
            currentLogStatus = packet;

            if (loggerGettingRecords) {
                numRecordsInLog = packet.getLog_NumOfRecords();
                loggerGetRecordPackets(numRecordsInLog);
            } else {
                sendEvent(_reactContext, "packetListener", packet.toJSON());
                toggleWriteCharNotifications(off);
            }

        }

        private void FillStatusData(StatusPacket packet) {
            Log.d(TAG, packet.toJSON().toString());
            sendEvent(_reactContext, "packetListener", packet.toJSON());
            //final DecimalFormat format = new DecimalFormat("#.##");
            //txtBatteryVolts.setText(format.format(packet.getVBatt()) + "V");
            //txtChargingState.setText(packet.getChargerState().toString());
        }

        private void FillVersionData(VersionPacket packet) {
            //txtFirmware.setText(packet.toString());
        }

        synchronized void FillStreamingData(final StreamingPacket packet) {

            if (isStreaming) {

                Log.d(TAG, "STREAMING " + packet.toJSON().toString());

                JSONObject packetJSON = packet.toJSON2();

                if (packetJSON != null) {

                    // Send packet to queue as JSONObject. Once there are PACKETS_IN_QUEUE (e.g. 10) in the queue,
                    // they're sent across to React Native
                    streamPacketQueue.put(packetJSON);

                }
            }

        }


        public class JSONArrayQueue  {
            private int limit;
            private JSONArray queue;

            public JSONArrayQueue(int limit) {
                this.limit = limit;
                queue = new JSONArray();
            }

            public void put(Object o)  {
                if (queue.length() == limit) {
                    sendEvent(_reactContext,"packetArrayListener", queue.toString());
                    //for (int i=1; i<=limit; i++) { queue.remove(i); }
                    queue = new JSONArray();
                }

                queue.put(o);

            }

        }


    }
}
