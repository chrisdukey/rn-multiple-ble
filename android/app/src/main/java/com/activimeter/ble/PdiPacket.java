package com.activimeter.ble;

import android.util.Log;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * A simple class to handle PDI packets
 */
public class PdiPacket  {
    private final static String TAG = PdiPacket.class.getSimpleName();

    // PDI commands
    public static final byte PDI_CMD_STREAMRECORD = (byte) 0x60;
    public static final byte PDI_CMD_STATUS = (byte) 0x30;
    public static final byte PDI_CMD_VERSION = (byte) 0x34;
    public static final byte PDI_CMD_CONFIG = (byte) 0x35;
    public static final byte PDI_CMD_LOGSTATUS = (byte) 0x58;

    public static final byte PDI_CMD_SETLED = (byte) 0x80;
    public static final byte PDI_CMD_STREAMENABLE = (byte) 0x63;
    public static final byte PDI_CMD_LOGGETFIRSTRECORD = (byte) 0x5a;
    public static final byte PDI_CMD_LOGGETRECORD = (byte) 0x5b;
    public static final byte PDI_CMD_LOGGETSTATUS = (byte) 0x58;
    public static final byte PDI_CMD_LOGENABLE = (byte) 0x5e;
    public static final byte PDI_CMD_LOGCLEAR = (byte) 0x59;

    // Command to reset the Pedometer
    public static final byte BLE_CMD_MPU_RESETPEDO = (byte) 0x49;

    // Commands to set and get the Real Time Clock on the SP-10C
    public static final byte BLE_CMD_SETRTC = (byte) 0x82;
    public static final byte BLE_CMD_GETRTC = (byte) 0x83;

    // Command to hopefully enable the pedometer
    public static final byte PDI_CMD_MPU_LOGDATA = (byte) 0x53;

    public static final byte PDI_SOP = (byte) 0xD1; // Byte representing start of packet
    public static final byte PDI_EOP = (byte) 0xDF; // Byte representing end of packet
    public static final byte PDI_STF = (byte) 0xDE; // Byte used for stuffing




    public static PdiPacket create(byte command, byte[] data, String deviceAddress) {

        try {
            switch (command) {
                case PDI_CMD_STREAMRECORD:
                    return new StreamingPacket(data, deviceAddress);
                case PDI_CMD_VERSION:
                    return new VersionPacket(data);
                case PDI_CMD_STATUS:
                    return new StatusPacket(data);
                case PDI_CMD_LOGSTATUS:
                    return new LogStatusPacket(data, deviceAddress);
                case PDI_CMD_LOGGETRECORD:
                    return new LogRecordPacket(data, deviceAddress);
                case  PDI_CMD_LOGGETFIRSTRECORD:
                    return new LogRecordPacket(data, deviceAddress);
                case  BLE_CMD_GETRTC:
                    return new ClockPacket(data, deviceAddress);
                default:
                    return new PdiPacket(command, data);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception parsing packet", e);
            return null;
        }
    }

    protected PdiPacket(byte command, byte[] _data) {
        Command = command;
        //Log.e(TAG, "PdiPacket data " + _data.toString());
        bytes = _data;
        data = ByteBuffer.wrap(bytes);
        data.order(ByteOrder.LITTLE_ENDIAN);
    }



    public byte Command;

    private byte[] bytes;
    protected ByteBuffer data;
}
