'use strict';
/**
 * This exposes the native RNMultipleBLE module as a JS module. 
**/
import { NativeModules } from 'react-native';
module.exports = NativeModules.RNMultipleBLE;
